#include "ScriptMgr.h"
#include "Player.h"


class player_login_custom : public PlayerScript
{
public:
    player_login_custom() : PlayerScript("player_login_custom") { }
    
    void OnLogin(Player* player, bool firstLogin)
    {
        if (player->getClass() == CLASS_PALADIN)
            player->RemoveSpell(21084);
        if (firstLogin)
        {
            SkillLineEntry const* SkillInfo = sSkillLineStore.LookupEntry(SKILL_FIRST_AID); // First aid
            uint16 skill_level = 225;
            if (!SkillInfo) { return; }

            player->SetSkill(SkillInfo->id, player->GetSkillStep(SkillInfo->id), skill_level, skill_level);
        }
    }
};

void AddSC_player_login_custom()
{
    new player_login_custom();
}